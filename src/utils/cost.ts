const userLang = navigator.language;

const formatter = new Intl.NumberFormat(userLang, {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2,
});

export default formatter.format;
