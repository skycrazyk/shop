import {
  Module,
  VuexModule,
  Mutation,
  Action,
} from 'vuex-module-decorators';

export interface ICatalogProduct {
  id: number;
  name: string;
  img: string;
  price: number;
}

export interface ICatalogState {
  products: ICatalogProduct[],
}

@Module({
  name: 'catalog',
  namespaced: true,
})
export default class Catalog extends VuexModule implements ICatalogState {
  products: ICatalogProduct[] = []

  @Mutation
  set(products: ICatalogProduct[]) {
    Object.assign(this.products, products);
  }

  /* eslint-disable class-methods-use-this */
  @Action({ commit: 'set' })
  async get() {
    const products = await fetch('catalog.json')
      .then(res => res.json())
      .then(catalog => catalog);

    return products;
  }
  /* eslint-enable */
}
