import { shallowMount } from '@vue/test-utils';
import ProductsItem from '@/components/ProductsItem.vue';
import { ICatalogProduct } from '@/store/catalog/';

describe('ProductsItem', () => {
  const product: ICatalogProduct = {
    id: 3,
    name: 'JS',
    img: 'catalog/js.png',
    price: 8.5,
  };

  const config = {
    propsData: {
      product,
    },
  };

  test('Image src is "catalog/js.png"', () => {
    const wrapper = shallowMount(ProductsItem, config);
    expect(wrapper.find('img').attributes().src).toBe('catalog/js.png');
  });

  test('Title is "JS"', () => {
    const wrapper = shallowMount(ProductsItem, config);
    expect(wrapper.html()).toContain('<div>JS</div>');
  });

  test('Price is "$8.50"', () => {
    const wrapper = shallowMount(ProductsItem, config);
    expect(wrapper.find('.price').text()).toBe('$8.50');
  });

  test('Button click emitted product', () => {
    const wrapper = shallowMount(ProductsItem, config);
    wrapper.find('.btn').trigger('click');
    const [[payload]] = wrapper.emitted().add;
    expect(payload).toEqual(product);
  });
});
