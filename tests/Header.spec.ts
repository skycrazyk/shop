import {
  shallowMount,
  createLocalVue,
  Stubs,
} from '@vue/test-utils';
import Vuex, { Store } from 'vuex';
import { RouteConfig } from 'vue-router';
import { IRootState } from '@/store/';
import Cart, { ICartState } from '@/store/cart/';
import Header from '@/components/Header.vue';

const localVue = createLocalVue();

localVue.use(Vuex);

interface Mocks {
  $route: RouteConfig,
}

describe('Header', () => {
  let store: Store<IRootState>;
  let state: ICartState;
  let mocks: Mocks;
  let stubs: Stubs;

  beforeEach(() => {
    state = {
      cart: [
        {
          id: 3,
          name: 'JS',
          img: 'catalog/js.png',
          price: 8.5,
          count: 1,
        },
        {
          id: 2,
          name: 'Webpack',
          img: 'catalog/webpack.png',
          price: 14.35,
          count: 1,
        },
      ],
    };
    store = new Vuex.Store({
      modules: {
        cart: {
          state,
          getters: Cart.getters,
          namespaced: true,
        },
      },
    });
    mocks = {
      $route: {
        path: '/',
      },
    };
    stubs = [
      'router-link',
      'router-view',
    ];
  });

  test('Menu has two elements', () => {
    const wrapper = shallowMount(Header, {
      store,
      localVue,
      mocks,
      stubs,
    });
    expect(wrapper.findAll('.item').length).toBe(2);
  });

  test('Header logo span', () => {
    const wrapper = shallowMount(Header, {
      store,
      localVue,
      mocks,
      stubs,
    });
    expect(wrapper.find('.logo span').is('span')).toBe(true);
  });

  test('Header logo router-link', () => {
    const wrapper = shallowMount(Header, {
      store,
      localVue,
      mocks: {
        $route: {
          path: '/some/path',
        },
      },
      stubs,
    });
    expect(wrapper.find('.logo router-link-stub').is('router-link-stub')).toBe(true);
  });

  test('Shopping cart counter is 2', () => {
    const wrapper = shallowMount(Header, {
      store,
      localVue,
      mocks,
      stubs,
    });
    expect(wrapper.find('.item:last-child router-link-stub').text()).toBe('Shopping cart (2)');
  });
});
