import { shallowMount, createLocalVue } from '@vue/test-utils';
import Vuex, { MutationTree, Store } from 'vuex';
import { ICartState } from '@/store/cart/';
import { ICatalogProduct } from '@/store/catalog/';
import { IRootState } from '@/store/';
import Products from '@/components/Products.vue';
import ProductsItem from '@/components/ProductsItem.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('Products', () => {
  const products: ICatalogProduct[] = [
    {
      id: 1,
      name: 'Node.js',
      img: 'catalog/nodejs.png',
      price: 5.5,
    },
    {
      id: 2,
      name: 'Webpack',
      img: 'catalog/webpack.png',
      price: 14.35,
    },
  ];

  const config = {
    propsData: {
      products,
    },
  };

  let mutations: MutationTree<ICartState>;
  let store: Store<IRootState>;

  beforeEach(() => {
    mutations = {
      increment: jest.fn(),
    };
    store = new Vuex.Store({
      modules: {
        cart: {
          mutations,
          namespaced: true,
        },
      },
    });
  });

  test('Two products are visible', () => {
    const wrapper = shallowMount(Products, config);
    expect(wrapper.findAll('.item').length).toBe(2);
  });

  test('Item event "add", executes method "increment" in store', () => {
    const wrapper = shallowMount(Products, {
      ...config,
      localVue,
      store,
    });
    wrapper.find(ProductsItem).vm.$emit('add');
    expect(mutations.increment).toHaveBeenCalled();
  });
});
