# Preview

[https://midnightcode.ru/static/stickers/#/](https://midnightcode.ru/static/stickers/#/)

# Technical specification

- На главной странице отображается список товаров с названием, изображением и ценой.
- Каждый товар можно тут же добавить в корзину, при чем не один раз.
- Вверху отображается ссылка на страницу "Корзина" со счетчиком количества заказанных товаров.
- На странице "Корзина" отображается список заказанных товаров, отсортированных по времени добавления (последний добавленный — вверху).
- Товары из корзины можно удалить (по одному или все сразу), а также поменять количество заказанных наименований товара.
- Внизу страницы отображается общая сумма заказа.

## Требования к функционалу:

- Все действия на сайте должны происходить без перезагрузки страницы.
- Интерфейс должен отвечать требованиям usability.
- После перезагрузки страницы состояние содержимого корзины должно быть сохранено.

## Инструменты:
- Framework Vue (Vue Route, Vuex).
- Preprocessors Pug, Stylus.
- TypeScript.
- Gitlab CI for build, test and deploy.

# Development

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
