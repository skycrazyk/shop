module.exports = {
  moduleFileExtensions: [
    'js',
    'ts',
    'json',
    'vue',
  ],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  transform: {
    '^.+\\.vue$': 'vue-jest',
    '^.+\\.js$': 'babel-jest',
    '^.+\\.ts$': 'ts-jest',
  },
  // transformIgnorePatterns: ['<rootDir>/node_modules/'],
  // collectCoverage: true,
  // collectCoverageFrom: [
  //   '**/src/**/*.{js,vue}',
  // ],
  // coverageReporters: ['html', 'text-summary'],
  // snapshotSerializers: [
  //   'jest-serializer-vue',
  // ],
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
};
